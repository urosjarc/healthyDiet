# General
 * Price	4.58
 * Energy	2656
 * Keto	32
 * PRAL	-87
 * Carbs	340
 * Proteins	72
 * Budwig	2329
 * Omega Ratio	1.21

# EAA & Minerals & Vitamins

Name | Status | Value(%)
 --- | --- | ---
Vitamin_B12 | :x: | 6
Calcium | :x: | 95
Zinc | :x: | 95
Sodium | :heavy_exclamation_mark: | 107
Thiamin | :heavy_exclamation_mark: | 124
Methionine | :heavy_exclamation_mark: | 127
Lysine | :heavy_exclamation_mark: | 136
Potassium | :heavy_exclamation_mark: | 143
Phenylalanine | :heavy_exclamation_mark: | 184
Folate | :heavy_exclamation_mark: | 188
Leucine | :heavy_exclamation_mark: | 193
Niacin | :heavy_check_mark: | 211
Iron | :heavy_check_mark: | 213
Histidine | :heavy_check_mark: | 220
Valine | :heavy_check_mark: | 224
Isoleucine | :heavy_check_mark: | 226
Selenium | :heavy_check_mark: | 232
Threonine | :heavy_check_mark: | 235
Phosphorus | :heavy_check_mark: | 240
Riboflavin | :heavy_check_mark: | 241
Magnesium | :heavy_check_mark: | 267
Vitamin_B6 | :heavy_check_mark: | 275
Tryptophan | :heavy_check_mark: | 333
Vitamin_C | :heavy_check_mark: | 355
Copper | :heavy_check_mark: | 431
Manganese | :heavy_check_mark: | 476
Vitamin_E | :heavy_check_mark: | 1653
Vitamin_K | :heavy_check_mark: | 4308
Vitamin_A | :heavy_check_mark: | 9503648303
