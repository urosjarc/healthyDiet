from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
from selenium import webdriver
import sys
import os
import os.path

this = sys.modules[__name__]
this.dir = os.path.dirname(os.path.realpath(__file__))
this.foodDir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')
this.browser = None


def init():
	chrome_options = webdriver.ChromeOptions()
	chrome_options.headless = True
	this.browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=f'{this.dir}/../chromedriver')


def plot(title, elements: list, self):
	fig, ax = plt.subplots()

	ax.bar([e[0] for e in elements], [getattr(self, e[0]) for e in elements])
	ax.set_title(title)
	ax.errorbar(x=[i for i in range(len(elements))], y=[e[1] for e in elements], yerr=10, fmt='.k')
	ax.set_xticklabels([e[0] for e in elements], rotation=30)

	imagePath = title + ".png"
	plt.savefig(imagePath)

	return f"\n![](/{imagePath}?raw=true)"


def massConverter(value, start='g', finish='g'):
	scale = {
		'g': 1,
		'mg': 10 ** -3,
		'mcg': 10 ** -6,
		'IU': 1
	}

	return value * scale[start] / scale[finish]


class Nutrient:

	Water: float = ('NUTRIENT_128', 'g')
	Total_Carbohydrate: float = ('NUTRIENT_4', 'g')
	Dietary_Fiber: float = ('NUTRIENT_5', 'g')

	Protein: float = ('NUTRIENT_77', 'g')
	Tryptophan: float = ('NUTRIENT_78', 'mg', 280)
	Threonine: float = ('NUTRIENT_79', 'mg', 1050)
	Isoleucine: float = ('NUTRIENT_80', 'mg', 1400)
	Leucine: float = ('NUTRIENT_81', 'mg', 2730)
	Lysine: float = ('NUTRIENT_82', 'mg', 2100)
	Methionine: float = ('NUTRIENT_83', 'mg', 1050)  # BUDWIG A.A.
	Cystine: float = ('NUTRIENT_84', 'mg')  # BUDWIG A.A.
	Phenylalanine: float = ('NUTRIENT_85', 'mg', 1750)
	Tyrosine: float = ('NUTRIENT_86', 'mg')
	Valine: float = ('NUTRIENT_87', 'mg', 1820)
	Arginine: float = ('NUTRIENT_88', 'mg')
	Histidine: float = ('NUTRIENT_89', 'mg', 700)
	Alanine: float = ('NUTRIENT_90', 'mg')
	Aspartic_acid: float = ('NUTRIENT_91', 'mg')
	Glutamic_acid: float = ('NUTRIENT_92', 'mg')
	Glycine: float = ('NUTRIENT_93', 'mg')
	Proline: float = ('NUTRIENT_94', 'mg')
	Serine: float = ('NUTRIENT_95', 'mg')
	Hydroxyproline: float = ('NUTRIENT_96', 'mg')

	Total_Fat: float = ('NUTRIENT_14', 'g')
	TotalOmega3: float = ('NUTRIENT_139', 'g')  # OMEGA RATIO
	TotalOmega6: float = ('NUTRIENT_140', 'g')  # OMEGA RATIO

	Vitamin_A: float = ('NUTRIENT_97', 'mcg', 700)
	Vitamin_C: float = ('NUTRIENT_100', 'mcg', 75000)
	Vitamin_D: float = ('NUTRIENT_101', 'mcg')
	Vitamin_E: float = ('NUTRIENT_102', 'mcg', 1500)
	Vitamin_K: float = ('NUTRIENT_103', 'mcg', 120)
	Thiamin: float = ('NUTRIENT_107', 'mcg', 1100)
	Riboflavin: float = ('NUTRIENT_108', 'mcg', 1100)
	Niacin: float = ('NUTRIENT_109', 'mcg', 14000)
	Vitamin_B6: float = ('NUTRIENT_110', 'mcg', 1200)
	Folate: float = ('NUTRIENT_111', 'mcg', 400)
	Vitamin_B12: float = ('NUTRIENT_115', 'mcg', 2.4)
	Pantothenic_acid: float = ('NUTRIENT_116', 'mcg')
	Choline: float = ('NUTRIENT_143', 'mcg')
	Betaine: float = ('NUTRIENT_144', 'mcg')

	Calcium = ('NUTRIENT_117', 'mg', 1200)  # PRAL LOAD
	Iron = ('NUTRIENT_118', 'mg', 18)
	Magnesium = ('NUTRIENT_119', 'mg', 420)  # PRAL LOAD
	Phosphorus = ('NUTRIENT_120', 'mg', 700)  # PRAL LOAD
	Potassium = ('NUTRIENT_121', 'mg', 4700)  # PRAL LOAD
	Sodium = ('NUTRIENT_122', 'mg', 1500)
	Zinc = ('NUTRIENT_123', 'mg', 13)
	Copper = ('NUTRIENT_124', 'mg', 0.9)
	Manganese = ('NUTRIENT_125', 'mg', 2.3)
	Selenium = ('NUTRIENT_126', 'mg', 0.055)
	Fluoride = ('NUTRIENT_145', 'mg')

	def potentialRenalAcidLoad(self):
		''' http://www.bitterpoison.com/archive/calculate-acid-alkaline-with-pral-formula/ '''

		return 0.49 * self.Protein + \
		       0.037 * self.Phosphorus - \
		       0.021 * self.Potassium - \
		       0.026 * self.Magnesium - \
		       0.013 * self.Calcium

	def energy(self):
		return self.Total_Carbohydrate * 4 + self.Protein * 4 + self.Total_Fat * 9

	def ketoRatio(self):
		return (self.Total_Fat * 9 / self.energy()) * 100

	def RDV(self):
		rdv = {}
		for attr, val in Nutrient.__dict__.items():
			if isinstance(val, tuple) and len(val) == 3:
				rdv[attr] = int(getattr(self, attr)*100 / float(val[-1]))

		return dict(sorted(rdv.items(), key=lambda x: x[1]))


class Food(Nutrient):

	def __init__(self, name, quantity, url, price=None):
		self.name = name
		self.quantity = quantity
		self.unit = None
		self.url = url
		self.price = 0 if price is None else (price[0] / price[1]) * quantity

		self.init()

	def init(self):

		foodFile = f'{this.foodDir}/{self.name}.html'
		if os.path.isfile(foodFile):
			with open(foodFile) as f:
				html = f.read()
		else:
			this.browser.get(self.url)
			html = this.browser.page_source
			with open(foodFile, 'w') as f:
				f.write(html)

		soup = BeautifulSoup(html, 'html.parser')

		self.name = soup.find('div', {'class': 'facts-heading'}).getText()

		# Get serving value in grams
		servings = 0
		form: BeautifulSoup = soup.find('select', {'name': 'serving'})
		for option in form.findAll('option'):
			if option.has_attr('selected'):
				servings = float(option.get('value'))

		for name, data in Nutrient.__dict__.items():
			if not isinstance(data, tuple):
				continue

			id = data[0]
			self.unit = data[1]

			quantity = soup.find('span', {'id': id}).getText()
			unit = soup.find('span', {'id': f'UNIT_{id}'}).getText()

			try:
				quantity = (float(quantity) / servings) * self.quantity  # Setting attributes for food quantity!
				quantity = massConverter(quantity, start=unit, finish=self.unit)
			except ValueError as err:
				quantity = 0

			setattr(self, name, quantity)


class Meal(Nutrient):
	def __init__(self, name, *foods):
		self.name = name
		self.foods = foods
		self.price = 0

		self.init()

	def init(self):
		for name, _ in Nutrient.__dict__.items():
			if not isinstance(_, tuple):
				continue

			quantity = 0
			for food in self.foods:
				try:
					quantity += getattr(food, name)
				except AttributeError as err:
					print(food)
					raise err

			setattr(self, name, quantity)

		for food in self.foods:
			self.price += food.price

class Meals(Nutrient):

	def __init__(self, *meals):
		self.meals = meals
		self.price = 0

		self.init()

	def init(self):
		for name, _ in Nutrient.__dict__.items():
			if not isinstance(_, tuple):
				continue

			quantity = 0
			for meal in self.meals:
				for food in meal.foods:
					quantity += getattr(food, name)

			setattr(self, name, quantity)

		for meal in self.meals:
			self.price += meal.price

	def report(self):
		return {
			'Price': round(self.price,2),
			'Energy': int(self.energy()),
			'Keto': int(self.ketoRatio()),
			'PRAL': int(self.potentialRenalAcidLoad()),
			'Carbs': int(self.Total_Carbohydrate - self.Dietary_Fiber),
			'Proteins': int(self.Protein),
			'Budwig':  int(self.Methionine + self.Cystine),
			'Omega Ratio': round(self.TotalOmega3/self.TotalOmega6, 2)
		}

if __name__ == '__main__':
	init()

	yogijski_obrok = Meal(
		'Yogijski obrok',

		# Baza beljakovin in energije (zdravljenje SIBO)
		Food(name='Parabol riz', quantity=200, price=(1.19, 500),
		     url="https://nutritiondata.self.com/facts/cereal-grains-and-pasta/5814/2"),

		# Pomemben vir B kompleksa
		Food(name='Pivski kvas', quantity=5, price=(5.30, 250), url="https://nutritiondata.self.com/facts/custom/1323569/2"),

		# Pomemben vir cinka
		Food(name='Bucina semena', quantity=25, price=(2.5, 200), url="https://nutritiondata.self.com/facts/nut-and-seed-products/3066/2"),

		# Vitamini, minerali, pral
		Food(name='Listi rdece pese', quantity=150,
		     url="https://nutritiondata.self.com/facts/vegetables-and-vegetable-products/2352/2"),
		Food(name='Blitva', quantity=150,
		     url="https://nutritiondata.self.com/facts/vegetables-and-vegetable-products/2399/2"),
		Food(name='Spinaca', quantity=150,
		     url="https://nutritiondata.self.com/facts/vegetables-and-vegetable-products/2626/2"),

		# OMEGA 3
		Food(name='Laneno olje', quantity=20, price=(7.16, 250),
		     url='http://nutritiondata.self.com/facts/fats-and-oils/7554/2'),
		Food(name='Ghee', quantity=15, price=(15.50, 580),
		     url='https://nutritiondata.self.com/facts/dairy-and-egg-products/133/2'),

		# ODSTRANJEVANJE BAKTERIJSKEGA BIO-FILMA KI SE USTVARI ZARADI POSTENJA
		Food(name="Olje crne kumine", quantity=5, price=(14.49, 250),
		     url="http://nutritiondata.self.com/facts/spices-and-herbs/184/2"),

		# USTVARJANJE BAKTERIJSKEGA RAVNOVESJA
		Food(name="Ginger", quantity=10, price=(3.99, 300),
		     url="http://nutritiondata.self.com/facts/vegetables-and-vegetable-products/2447/2")
	)

	meals = Meals(yogijski_obrok, yogijski_obrok)

	readme = "# General\n"
	for name, val in meals.report().items():
		readme += f" * {name}\t{val}\n"

	readme += "\n# EAA & Minerals & Vitamins\n\nName | Status | Value(%)\n --- | --- | ---\n"
	for name, val in meals.RDV().items():

		status = ':heavy_check_mark:'
		if 100 < val < 200:
			status = ':heavy_exclamation_mark:'
		elif val < 100:
			status = ':x:'

		readme += f"{name} | {status} | {val}\n"

	with open('README.md', 'w') as f:
		f.write(readme)
