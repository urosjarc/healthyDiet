include config/Makefile

#============================
### START DEVELOPING ########
#============================

setup: clean install ## execute setup script

setup-venv: clean-venv ## setup virtual environment
	python3.6 -m venv $(VIRTUAL_ENV)

install: setup-venv install-dep ## start virtual environment and install dev. requirements

install-dep: clean-py ## install development libs
	$(PIP) install --no-cache-dir -e .
	$(PIP) install setuptools --upgrade
	$(PIP) install --no-cache-dir -e ".[develop]"

#============================
### RUNNING #################
#============================

run: ## run package script
	$(PY) $(PACKAGE)

#============================
### CLEANING ################
#============================

clean: clean-venv clean-py # clean all setup files/folders

clean-venv: clean-py #clean virtual environment folder
	rm -fr $(VIRTUAL_ENV)

clean-py: clean-build clean-pyc ## remove all Python files/folders


clean-build: ## remove build artifacts
	rm -fr build
	rm -fr dist
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +
	find . -name '*.tar.gz' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +