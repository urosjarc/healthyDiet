#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup
import foods

with open('README.md') as readme_file:
	readme = readme_file.read()

requirements = [
	'beautifulsoup4',
	'selenium',
	'matplotlib'
]

tests_require = [ ]

setup_requires = [
	'setuptools',
	'pip',
	'virtualenv',
	'wheel',
	'bumpversion'
]

setup(
	name=foods.__name__,
	long_description=readme,
	install_requires=requirements,
	setup_requires=setup_requires,
	tests_require=tests_require,
	extras_require={
		'develop': tests_require + setup_requires,
	}
)